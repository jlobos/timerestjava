package cl.tuten.timeservicetest;


import static io.restassured.RestAssured.post;
import static org.hamcrest.CoreMatchers.equalTo;
 
import org.junit.BeforeClass;
import org.junit.Test;
 
import io.restassured.RestAssured;
 
public class CalcTimeServiceTest {
    @BeforeClass
    public static void init() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }
 
    @Test
    public void testTimeSuccess() {
    	
    	post("/TimeService/rest/time/01:34:38/-3")
    	.then()
        .body("response.time", equalTo("04:34:38"))
        .body("response.timezone", equalTo("utc"));
    }
    
    @Test
    public void testZeroSuccess() {
    	
    	post("/TimeService/rest/time/01:34:38/0")
    	.then()
        .body("response.time", equalTo("01:34:38"))
        .body("response.timezone", equalTo("utc"));
    }
    
    @Test
    public void testOutRange() {
    	
    	post("/TimeService/rest/time/01:34:38/22")
    	.then()
        .body("response.error", equalTo("Bad Request"));
    }
}