package cl.tuten.timeservice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


@Path("/time")
public class CalcTimeService {

	
	@Path("{hour}/{timezone}/utc")
	@POST
	@Produces("application/json")
	public Response getTime_JSON(
			@PathParam("hour") String time,//
	        @PathParam("timezone") int timezone,
	        @PathParam("utc") String utc
	        ) {
	 

		try {
			LocalTime timePoint = LocalTime.parse(time);
			
			
			Instant instant = timePoint.atDate(LocalDate.of(1900, 1, 1)).
			        atZone(ZoneId.systemDefault()).toInstant();
			Date timeConverter = Date.from(instant);
			
			
			DateFormat converter = new SimpleDateFormat("HH:mm:ss");
			converter.setTimeZone(TimeZone.getTimeZone("UTC"));
			String hourConverter = converter.format(timeConverter);

			Object entity ="{\r\n" + 
			"\"response\": {\r\n" + 
			"\"time\": \""+ hourConverter+ "\",\r\n" + 
			"\"timezone\": \""+TimeZone.getTimeZone("UTC").getID()+"\"\r\n" + 
			"}\r\n" + 
			"}";
			
			// System.out.println(entity);
			return Response.status(Response.Status.OK).entity(entity)
					.header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                    .build();
		}
		catch (Exception e) {
			Object entity = "{\r\n" + 
					"    \"response\": {\r\n" + 
					"        \"error\": \"Bad Request\"\r\n" + 
					"    }\r\n" + 
					"}";
			return Response.status(Response.Status.BAD_REQUEST).entity(entity).build();
		}
	 
	   }
	
	
	@Path("{hour}/{timezone}")
	@POST
	@Produces("application/json")
	public Response getTimeUTC_JSON(
			@PathParam("hour") String time,//
	        @PathParam("timezone") int timezone) {
	 
		try {
				if(timezone <= 14 && timezone >= -11) {
					Calendar cal = Calendar.getInstance();
					
					
					// Desplegamos la fecha
					Date tempDate = cal.getTime();
					
					LocalTime timePoint = LocalTime.parse(time);
					
					Instant instant = timePoint.atDate(LocalDate.of(1970, 1, 1)).
					        atZone(ZoneId.systemDefault()).toInstant();
					Date timeConverter = Date.from(instant);
					
					cal.setTime(timeConverter);
					
					cal.set(Calendar.HOUR, cal.get(Calendar.HOUR)+ ( timezone * -1));
					
					tempDate = cal.getTime();
					
					DateFormat converter = new SimpleDateFormat("HH:mm:ss");
					// converter.setTimeZone(TimeZone.getTimeZone("UTC"));
					String hourConverter = converter.format(tempDate);
		
					Object entity ="{\r\n" + 
					"\"response\": {\r\n" + 
					"\"time\": \""+ hourConverter+ "\",\r\n" + 
					"\"timezone\": \"utc\"\r\n" + 
					"}\r\n" + 
					"}";
					
					// System.out.println(entity);
					return Response.status(Response.Status.OK).entity(entity)
							.header("Access-Control-Allow-Origin", "*")
		                    .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
		                    .build();
			}
			else {
				throw new Exception();
				
			}
		}
		catch (Exception e) {
			Object entity = "{\r\n" + 
					"    \"response\": {\r\n" + 
					"        \"error\": \"Bad Request\"\r\n" + 
					"    }\r\n" + 
					"}";
			return Response.status(Response.Status.BAD_REQUEST).entity(entity).build();
		}
	 
	   }

}
